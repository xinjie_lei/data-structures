#include<stdio.h>
#include<stdlib.h>
#include "sorting.h"

long * Load_File(char * Filename, int * Size)
{
  FILE * fh = NULL;
  long * array = NULL;
  int n = 0;

  if((fh = fopen(Filename, "r")) == NULL)
    {
      return NULL;
    }
  if(fscanf(fh, "%d", Size) != 0)
    {
      array = malloc(sizeof(long) * (*Size));
      for(n = 0; n < (*Size); n++)
	{
	  if((fscanf(fh, "%ld", &array[n])) == 0)
	    {
	      return NULL;
	    }
	}
      fclose(fh);
      return array;
    }
  else
    {
      fclose(fh);
      return NULL;
    }
}
int Save_File(char * Filename, long * Array, int Size)
{
  FILE * fh = NULL;
  int total = 0;
  int n = 0;
  fh = fopen(Filename, "w");
  fprintf(fh, "%d\n", Size);
  for(n = 0; n < Size; n++)
    {
      total = fprintf(fh, "%ld\n", Array[n]) + total;
    }

  fclose(fh);

  return total;
}

int Print_Seq(char * Filename, int Size)
{
  FILE * fh = NULL;
  int n = 0;
  int size = 0;
  int p = 0;
  int q = 0;
  int i = 0;
  int pow_2 = 1;
  int pow_3 = 1;
  int temp = 0;
  fh = fopen(Filename, "w");
  while(pow_3 < Size)
    {
      pow_3 *= 3;
      i++;
    }
  size = i;
  fprintf(fh, "%d\n", i * (i+1) / 2);
  for(n = 0; n < size; n++)
    {
      for(p = n,q = 0;p >= 0; p--,q++)
	{
	  pow_2 = 1;
	  for(i = 0; i < p; i++)
	    {
	      pow_2 *= 2;
	    }
	  pow_3 = 1;
	  for(i = 0; i < q; i++)
	    {
	      pow_3 *= 3;
	    }
	  temp = pow_2 * pow_3;
	  fprintf(fh, "%d\n", temp);
	}
    }
  fclose(fh);
  return size * (size + 1) / 2;
}

int * generateSeq(int Size, int * num)
{
  int n = 0;
  int size = 0;
  int p = 0;
  int q = 0;
  int i = 0;
  int pow_2 = 1;
  int pow_3 = 1;
  int temp = 0;
  int * seq = NULL;
  int ind = 0;
  while(pow_3 < Size)
    {
      pow_3 *= 3;
      i++;
    }
  size = i;
  (*num) = i * (i+1) / 2 - 1;
  seq = malloc(sizeof(int) * i * (i + 1) / 2);
  for(n = 0; n < size; n++)
    {
      for(p = n,q = 0;p >= 0; p--,q++)
	{
	  pow_2 = 1;
	  for(i = 0; i < p; i++)
	    {
	      pow_2 *= 2;
	    }
	  pow_3 = 1;
	  for(i = 0; i < q; i++)
	    {
	      pow_3 *= 3;
	    }
	  temp = pow_2 * pow_3;
	  seq[ind] = temp;
	  ind++;
	}
    }
  return seq;
}

void Shell_Insertion_Sort(long *Array, int Size, double * N_Comp, double * N_Move)
{
  int * seq = NULL;
  int ind = 0;
  int k = 0;
  int j = 0;
  int i = 0;
  int temp = 0;
  seq = generateSeq(Size, &ind);
  while(ind >= 0)
    {
      k = seq[ind];
      for(j = k; j < Size; j++)
	{
	  temp = Array[j];
	  (*N_Move)++;
	  i = j;
	  while(i >= k && Array[i - k] > temp)
	    {
	      (*N_Comp)++;
	      Array[i] = Array[i - k];
	      (*N_Move)++;
	      i = i - k;
	    }
	  Array[i] = temp;
	  (*N_Move)++;
	}
      ind--;
    }
  free(seq);
}
void Shell_Selection_Sort(long *Array, int Size, double * N_Comp, double * N_Move)
{
  int * seq =  NULL;
  int ind = 0;
  int k = 0;
  int last_ind = 0;
  int j = 0;
  int i = 0;
  int minloc = 0;
  int temp = 0;
  seq = generateSeq(Size, &ind);
  while(ind >= 0)
    {
      k = seq[ind];
      for(j = 0; j < k; j++)
	{
	  last_ind = findLastind(j, k, Size);
	  for(i = j; i < last_ind; i +=k)
	    {
	      minloc = findMin(Array, i, last_ind, k, N_Comp);
	      if (i != minloc)
		{ 
		  temp = Array[i];
		  Array[i] = Array[minloc];
		  Array[minloc] = temp;
		  (*N_Move)+=3;
		}
	    }        
	}
      ind--;
    }
  free(seq);
}

int findLastind(int j, int k, int Size)
{
  int i = 0;
  while((j + i*k) < Size)
    {
      i++;
    }
  return j+(i-1)*k;
}

int findMin(long * Array, int startLoc, int endLoc, int k, double * N_Comp)
{
  int i;
  int  minloc;
	
  minloc = startLoc;
  for (i = startLoc+k; i <= endLoc; i+=k)
    {
      if (Array[i] < Array[minloc])
	{ 
	  (*N_Comp) ++;
	  minloc = i;
	}
    }	
  return minloc;
}
