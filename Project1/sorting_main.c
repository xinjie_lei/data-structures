#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include<math.h>
#include "sorting.h"

int main(int argc, char ** argv)
{
  double time_load = 0;
  double time_save = 0;
  double time_print = 0;
  double time_sort = 0;
  int Size = 0;
  long * array = NULL;
  double N_Comp = 0;
  double N_Move = 0;
  if(argc < 5)
    {
      printf("usage: ./proj1 i/s input.txt seq.txt output.txt");
      return EXIT_FAILURE;
    }
 
  time_load = clock();
  array = Load_File(argv[2], &Size);
  time_load = clock() - time_load;
  if((*argv[1]) == 'i')
    {

      time_print = clock();
      Print_Seq(argv[3], Size);
      time_print = clock() - time_print;
      time_sort = clock();
      Shell_Insertion_Sort(array, Size, &N_Comp, &N_Move);
      time_sort = clock() - time_sort;

      time_save = clock();
      Save_File(argv[4], array, Size);
      time_save = clock() - time_save;
      printf("Number of comparisons: %le\n", N_Comp);
      printf("Number of moves: %le\n", N_Move);
      printf("I/O time: %le\n", (time_load + time_print + time_save) / CLOCKS_PER_SECOND);
      printf("Sorting Time: %le\n", time_sort/CLOCKS_PER_SECOND);
    }
  if((*argv[1]) == 's')
    {

      time_print = clock();
      Print_Seq(argv[3], Size);
      time_print = clock() - time_print;
      time_sort = clock();
      Shell_Selection_Sort(array, Size, &N_Comp, &N_Move);
      time_sort = clock() - time_sort;
    
      time_save = clock();
      Save_File(argv[4], array, Size);
      time_save = clock() - time_save;
      printf("Number of comparisons: %le\n", N_Comp);
      printf("Number of moves: %le\n", N_Move);
      printf("I/O time: %le\n", (time_load + time_print + time_save) / CLOCKS_PER_SECOND);
      printf("Sorting time: %le\n", time_sort/CLOCKS_PER_SECOND);
    }
  free(array);
  return EXIT_SUCCESS;
}
