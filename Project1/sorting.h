#ifndef SORTING_H
#define SORTING_H
#define CLOCKS_PER_SECOND 1000000

long * Load_File(char * Filename, int * Size);
int Save_File(char * Filename, long * Array, int Size);
int * generateSeq(int Size, int * num);
void Shell_Insertion_Sort(long *Array, int Size, double * N_Comp, double * N_Move);
void Shell_Selection_Sort(long *Array, int Size, double * N_Comp, double * N_Move);
int Print_Seq(char * Filename, int Size);
int findLastind(int j, int k, int Size);
int findMin(long * Array, int startLoc, int endLoc, int k, double * N_Comp);

#endif
