#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "river.h"

int main(int argc, char ** argv)
{
  //Declarations
  Graph * graph;
  int turn = 0;

  //Statements
  if(argc != 2)
    {
        printf("Usage : ./proj4 input_file output_file ");
        return EXIT_FAILURE;
    }
  graph = load_file(argv[1]);
  turn = find_turn(graph);

  printf("%d\n", turn);

  destroy_graph(graph);

  return EXIT_SUCCESS;
}

Graph * load_file(char * filename)
{
  //Declarations
  Graph * graph = NULL;
  FILE * fh = NULL;
  int N;
  int * temp_edge = NULL;
  char c;
  int num_of_black = 0;

  //Statements
  fh = fopen(filename, "r");
  graph = malloc(sizeof(Graph));
  if(fscanf(fh, "%d", &N))
    {}
  if(fscanf(fh, "%c", &c))
    {}
  temp_edge = load_edge(fh, N);
  num_of_black = count_black(temp_edge, N);

  graph -> V = N * (N - 1) * 2 + 2;
  graph -> E = 2 * ((N -1) * (N - 1) * 4 + (N - 1) + num_of_black);

  graph -> edge = malloc(sizeof(Edge) * graph -> E);
  graph -> vert = malloc(sizeof(Vert) * graph -> V);

  construct_vert(graph, temp_edge, N);
  construct_edge_sd(graph, N);
  construct_edge_w(graph);

  free(temp_edge);
  fclose(fh);
  return graph;
}
int * load_edge(FILE * fh, int N)
{
  //Declarations
  int i =  0;
  int total = N * (N-1);
  int * temp_edge;
  char c;

  //Statements
  temp_edge = malloc(sizeof(int) * total);
  for(i = 0; i < total; i++)
    {
      if(fscanf(fh, "%c", &c))
	{
	  if(c == '0')
	    {
	      temp_edge[i] = 0;
	    }
	  else if(c == '1')
	    {
	      temp_edge[i] = 1;
	    }
	  else
	  {
	    i--;
	  }
	}
    }

  return temp_edge;
}

int count_black(int * arr, int N)
{
  //Declarations
  int i = 0;
  int j = 0;
  int total = N * (N - 1);
  int num = 0;

  //Statements
  for(i = 0; i < N; i++)
    {
      for(j = i+N; j < total; j = j+N)
	{
	  if(arr[j-N]==1 && arr[j] == 1)
	    {
	      num++;
	    }
	}
    }
  return num;
}
void construct_vert(Graph * graph, int * temp_edge, int N)
{
  //Declarations
  int i = 0;
  int j = 0;
  int k = 0;
  int shift = 2 * N - 1;

  //Statements
  for(i = 0; i < graph -> V; i++)
    {
      graph -> vert[i].turn = 0;
      if(i > j* shift && i < j*shift + N)
	{
	  if(temp_edge[j + k * N] == 1)
	    {
	      graph -> vert[i].color = "BLACK";
	    }
	  else
	    {
	      graph -> vert[i].color = "WHITE";
	    }
	  k++;
	}
      else
	{
	  graph -> vert[i].color = "WHITE";
	}
      if(k == N-1)
	{
	  k = 0;
	  j++;
	}
    }
}

void construct_edge_sd(Graph * graph, int N)
{
  //Declarations
  int ind_vert = 0;
  int ind_edge = 0;
  int temp = 0;
  int j = 0;
  int k = 0;
  int i = 0;	

  //Statements
  for(ind_vert = 0; ind_vert < graph -> V - 1; ind_vert ++)
    {
      if(ind_vert == 0)
	{
	  for(ind_edge = 0; ind_edge < N - 1; ind_edge++)
	    {
	      graph -> edge[ind_edge].src = ind_vert;
	      graph -> edge[ind_edge].dest = ind_edge+1;
	    }
	  j=1;
	}
      else if(ind_vert == graph -> V - N)
	{
      temp = ind_edge;
	  for(ind_edge = temp; ind_vert < graph -> V - 1; ind_edge++,ind_vert++)
	    {
	      graph -> edge[ind_edge].src = ind_vert;
	      graph -> edge[ind_edge].dest = graph -> V - 1;
	    }
	  ind_vert = ind_vert - N  + 1;
	  for(k = 0; k < N -2; k++)
	    {
	      if(strcmp(graph -> vert[ind_vert].color,"BLACK") == 0 && strcmp(graph -> vert[ind_vert+1].color, "BLACK") == 0)
		{
		  graph -> edge[ind_edge].src = ind_vert;
		  graph -> edge[ind_edge].dest = ind_vert + 1;
		  ind_edge++;
		  graph -> edge[ind_edge].src = ind_vert + 1;
		  graph -> edge[ind_edge].dest = ind_vert;
		  ind_edge++;
		}
	      ind_vert++;
	    }
	}
      else
	{
	  if(j %2 == 1)
	    {
	      for(k = 0; k< N -1 ; k++)
		{
		  for(i =1;i>-1;i--,ind_edge++)
		    {
		      graph -> edge[ind_edge].src = ind_vert;
		      graph -> edge[ind_edge].dest = ind_vert + N - i;
		      ind_edge++;
		      graph -> edge[ind_edge].src = ind_vert + N - i;
		      graph -> edge[ind_edge].dest = ind_vert;
		    }
		  ind_vert++;
		}
	      j++;
	      ind_vert = ind_vert - N  + 1;
	      for(k = 0; k < N -2; k++)
		{
		  if(strcmp(graph -> vert[ind_vert].color,"BLACK") == 0 && strcmp(graph -> vert[ind_vert+1].color, "BLACK") == 0)
		    {
		      graph -> edge[ind_edge].src = ind_vert;
		      graph -> edge[ind_edge].dest = ind_vert + 1;
		      ind_edge++;
		      graph -> edge[ind_edge].src = ind_vert + 1;
		      graph -> edge[ind_edge].dest = ind_vert;
		      ind_edge++;
		    }
		  ind_vert++;
		}
	    }
	  else
	    {
	      for(k = 0; k< N; k++)
		{
		  if(k == 0)
		    {
		      graph -> edge[ind_edge].src = ind_vert;
		      graph -> edge[ind_edge].dest = ind_vert + N;
		      ind_edge++;
		      graph -> edge[ind_edge].src = ind_vert + N;
		      graph -> edge[ind_edge].dest = ind_vert;
		      ind_edge++;
		    }
		  else if(k == N-1)
		    {
		      graph -> edge[ind_edge].src = ind_vert;
		      graph -> edge[ind_edge].dest = ind_vert + N - 1;
		      ind_edge++;
		      graph -> edge[ind_edge].src = ind_vert + N -1;
		      graph -> edge[ind_edge].dest = ind_vert;
		      ind_edge++;
		    }
		  else
		    {
		      for(i =1;i>-1;i--, ind_edge++)
			{
			  graph -> edge[ind_edge].src = ind_vert;
			  graph -> edge[ind_edge].dest = ind_vert + N - i;
			  ind_edge++;
			  graph -> edge[ind_edge].src = ind_vert + N -i;
			  graph -> edge[ind_edge].dest = ind_vert;
			}
		    }
		  ind_vert++;
		}
	      j++;
	      ind_vert--;
	    }
	}
    }
}
void construct_edge_w(Graph * graph)
{
  //Declarations
  int i = 0;

  //Statements
  for(i = 0; i< graph -> E;i++)
    {
      if(strcmp(graph -> vert[graph->edge[i].dest].color, "BLACK") == 0)
	{
	  graph -> edge[i].weight = 0; 
	}
      else
	{
	  graph -> edge[i].weight = 1; 
	}
    }
}

int find_turn(Graph* graph)
{
  //Declarations
  int V = graph-> V;
  int E = graph-> E;
  int dist[V];
  int i = 0;
  int j = 0;

  //Statements
  for (i = 0; i < V; i++)
    {
      dist[i] = INT_MAX;
    }
  dist[0] = 0;
  
  for (i = 1; i < V; i++)
    {
      for (j = 0; j < E; j++)
	{
	  if (dist[graph->edge[j].src] + graph->edge[j].weight < dist[graph->edge[j].dest])
	    {
	      dist[graph->edge[j].dest] = dist[graph->edge[j].src] + graph->edge[j].weight;
	    }
	}
    }
  
  return dist[V-1];
}


void destroy_graph(Graph * graph)
{
  //Statements
  free(graph -> edge);
  free(graph -> vert);
  free(graph);
}
