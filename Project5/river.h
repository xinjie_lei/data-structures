#ifndef RIVER_H
#define RIVER_H
#define CLOCKS_PER_SECOND 1000000
#define INT_MAX 2048 * 2048

typedef struct _Graph
{
  int V;
  int E;
  struct _Edge * edge;
  struct _Vert * vert;
}Graph;

typedef struct _Edge 
{
  int src;
  int dest;
  int weight;
}Edge;

typedef struct _Vert
{
  int turn;
  char * color;
}Vert;

Graph * load_file(char * filename);
int * load_edge(FILE * fh, int N);
int count_black(int * arr, int N);
void construct_vert(Graph * graph, int * temp_edge, int N);
void construct_edge_sd(Graph * graph, int N);
void construct_edge_w(Graph * graph);
int find_turn(Graph * graph);
void destroy_graph(Graph * graph);
#endif
