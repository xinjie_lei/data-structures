#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include "packing.h"

int main(int argc, char ** argv)
{
  //Declarations
  TreeNode * tree = NULL;
  TreeNode * root = NULL;
  TreeNode * found = NULL;
  int largest_index = 0;
  double time_pack = 0;

  //Statements
  if(argc != 3)
    {
      printf("usage: ./proj3 input_file output_file");
      return EXIT_FAILURE;
    }
  //load file
  tree = load_file(argv[1], &largest_index);

  //perform packing
  time_pack = clock();
  root = tree;
  tree_pack(root);
  time_pack = clock() - time_pack;

  //save file
  root = tree;
  save_file(argv[2], root, largest_index);

  //Report
  root = tree;
  printf("Preorder:  ");
  pre_order(root);
  printf("\n\n");
  root = tree;
  printf("Inorder:  ");
  in_order(root);
  printf("\n\n");
  root = tree;
  printf("Postorder:  ");
  post_order(root);
  printf("\n\n");

  printf("Width: %le\n", tree -> width);
  printf("Height: %le\n\n", tree -> height);
  found = tree_search_key(root, largest_index);
  printf("X-coordinate: %le\n", found -> xcoord);
  printf("Y-coordinate: %le\n\n", found -> ycoord);

  printf("Elapsed Time: %le\n", time_pack/CLOCKS_PER_SECOND);
  tree_destruct(tree);

  return EXIT_SUCCESS;
}

TreeNode * load_file(char * filename, int * largest_index)
{
  //Declarations
  FILE * fh = NULL;
  int total;
  int i;
  int thisnode; 
  int parnode; 
  int lcnode; 
  int rcnode; 
  char cutline; 
  char c_width;
  char c_height;
  double width; 
  double height;
  TreeNode * root = NULL;
  ListNode * head = NULL;
  ListNode * temp = NULL;

  //Statements
  fh = fopen(filename, "r");
  if(fh ==  NULL)
    {
      fclose(fh);
      return NULL;
    }
  if(fscanf(fh, "%d", largest_index) != 0)
    {}
  if(fscanf(fh, "%d", &total) != 0)
    {}

  for(i = 0; i < total; i++)
    {
      if(i < *largest_index)
	{
	  if(fscanf(fh,"%d %d %d %d %c %le %le", &thisnode, &parnode, &lcnode, &rcnode, &cutline, &width, &height) != 0)
	    {}
	  head = list_insert(head, thisnode, parnode, lcnode, rcnode, cutline, width, height);
	}
      else
	{
	  if(fscanf(fh,"%d %d %d %d %c %c %c", &thisnode, &parnode, &lcnode, &rcnode, &cutline, &c_width, &c_height) != 0)
	    {}
	  head = list_insert(head, thisnode, parnode, lcnode, rcnode, cutline, 0, 0);
	}
    }
  temp = head;
  for(i = 0; i< total;i++)
    {
      root = tree_insert(root, temp);
      temp = temp -> next;
    }

  list_destruct(head);
  fclose(fh);
  return root;
}

ListNode * list_construct(int thisnode, int parnode, int lcnode, int rcnode, char cutline, double width, double height)
{
  //Declarations
  ListNode * node = NULL;

  //Statements
  node = malloc(sizeof(ListNode));
  node -> thisnode = thisnode; 
  node -> parnode = parnode;
  node -> lcnode = lcnode;
  node -> rcnode = rcnode;
  node -> cutline = cutline;
  node -> width = width;
  node -> height = height;
  node -> next = NULL;

  return node;
}

ListNode * list_insert(ListNode *head, int thisnode, int parnode, int lcnode, int rcnode, char cutline, double width, double height)
{
  //Declarations
  ListNode * node = NULL;

  //Statements
  node = list_construct(thisnode, parnode, lcnode, rcnode, cutline, width, height);
  node -> next = head;
  return node;
}

void list_destruct(ListNode * head)
{
  //Declarations
  ListNode * temp = NULL;

  //Statements
  while(head != NULL)
    {
      temp = head -> next;
      free(head);
      head = temp;
    }
}
TreeNode * tree_construct(ListNode * head)
{
  //Declarations
  TreeNode * node = NULL;

  //Statements
  node = malloc(sizeof(TreeNode));
  node -> thisnode = head -> thisnode; 
  node -> parnode = head -> parnode;
  node -> lcnode = head -> lcnode;
  node -> rcnode = head -> rcnode;
  node -> cutline = head -> cutline;
  node -> width = head -> width;
  node -> height = head -> height;
  node -> xcoord = 0;
  node -> ycoord = 0;
  node -> left = NULL;
  node -> right = NULL;

  return node;
}

TreeNode * tree_insert(TreeNode * root, ListNode * head)
{
  //Declarations
  TreeNode * node = NULL; 
  TreeNode * parent = NULL;
  TreeNode * tree = root;

  //Statements
  node =  tree_construct(head);
  if(root == NULL)
    {
      root = node;
    }
  else
    {
      parent = tree_search_par(tree, node -> parnode);
      if(node -> thisnode == parent -> lcnode)
	{
	  parent -> left = node;
	}
      else
	{
	  parent -> right = node;
	}
    }
  return root;
}
TreeNode * tree_search_par(TreeNode * root, int parnode)
{
  //Declarations
  TreeNode * found = NULL;

  //Statements
  if(root == NULL)
    {
      return NULL;
    }
  if(root -> thisnode == parnode)
    {
      return root;
    }
  found = tree_search_par(root -> left, parnode);
  if(found != NULL)
    {
      return found;
    }
  else
    {
      found = tree_search_par(root -> right, parnode);
      return found;
    }
}
void tree_pack(TreeNode * tree)
{
  //Declarations
  TreeNode * root = tree;

  //Statements
  rectangle_construct(root);
  root = tree;
  get_coordinate(root, NULL);
  return;
}
void rectangle_construct(TreeNode * root)
{
  //Statements
  if(root -> cutline == '-')
    {
      return;
    }
  rectangle_construct(root -> left);
  rectangle_construct(root -> right);
  if(root -> cutline == 'V')
    {
      root -> width = root -> left -> width +  root -> right -> width;
      if(root -> left -> height > root -> right -> height)
	{
	  root -> height = root -> left -> height;
	}
      else
	{
	  root -> height = root -> right -> height;
	}
    }
  else
    {
      root -> height = root -> left -> height +  root -> right -> height;
      if(root -> left -> width > root -> right -> width)
	{
	  root -> width = root -> left -> width;
	}
      else
	{
	  root -> width = root -> right -> width;
	}
    }
}
void get_coordinate(TreeNode * root, TreeNode * parent)
{

  //Statements
  if(root == NULL)
    {
      return;
    }
  if(parent == NULL)
    {
      root -> xcoord = 0;
      root -> ycoord = 0;
    }
  else
    {
      if(parent -> cutline == 'H')
	{
	  if(root -> thisnode == parent -> lcnode)
	    {
	      parent -> left -> xcoord = parent -> xcoord;
	      parent -> left -> ycoord = parent -> ycoord +  parent -> height - parent -> left -> height;
	    }
	  else
	    {
	      parent -> right -> xcoord = parent -> xcoord;
	      parent -> right -> ycoord = parent -> ycoord;
	    }
	}
      else
	{
	  if(root -> thisnode == parent -> lcnode)
	    {
	      parent -> left -> xcoord = parent -> xcoord;
	      parent -> left -> ycoord = parent -> ycoord;
	    }
	  else
	    {
	      parent -> right -> xcoord = parent -> xcoord + parent -> left -> width;
	      parent -> right -> ycoord = parent -> ycoord;
	    }
	}
    }
  get_coordinate(root -> left, root);
  get_coordinate(root -> right, root);
}
void save_file(char * filename, TreeNode * tree, int total)
{
  //Declarations
  FILE * fh = NULL;
  fh = fopen(filename, "w");
  int i = 0;
  TreeNode * found = NULL;
  TreeNode * root = tree;

  //Statements
  fprintf(fh, "%d\n", total);
  for(i = 1; i <=total; i++)
    {
      found = tree_search_key(root, i);
      fprintf(fh, "%d %le %le %le %le\n", found -> thisnode, found -> width, found -> height, found -> xcoord, found -> ycoord);
      root = tree;
    }

  fclose(fh);
  return;
}

TreeNode * tree_search_key(TreeNode * root, int i)
{
  //Declarations
  TreeNode * found = NULL;

  //Statements
  if(root == NULL)
    {
      return NULL;
    }
  if(root -> thisnode == i)
    {
      return root;
    }
  found = tree_search_key(root -> left, i);
  if(found != NULL)
    {
      return found;
    }
  else
    {
      found = tree_search_key(root -> right, i);
      return found;
    }
}
void tree_destruct(TreeNode * tree)
{
  //Statements
  if(tree == NULL)
    {
      return;
    }
  tree_destruct(tree -> left);
  tree_destruct(tree -> right);
  free(tree);
}
void pre_order(TreeNode * tree)
{
  //Statements
 if(tree == NULL)
    {
      return;
    }
 printf("%d ", tree -> thisnode);
 pre_order(tree -> left);
 pre_order(tree -> right);
}
void in_order(TreeNode * tree)
{
  //Statements
  if(tree == NULL)
    {
      return;
    }
  in_order(tree -> left);
  printf("%d ", tree -> thisnode);
  in_order(tree -> right);
}
void post_order(TreeNode * tree)
{
  //Statements
  if(tree == NULL)
    {
      return;
    }
  post_order(tree -> left);
  post_order(tree -> right);
  printf("%d ", tree -> thisnode);
}
