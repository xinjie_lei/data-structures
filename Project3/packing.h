#ifndef PACKING_H
#define PACKING_H
#define CLOCKS_PER_SECOND 1000000

typedef struct _tree
{
  int thisnode;
  int parnode;
  int lcnode;
  int rcnode;
  char cutline;
  double width;
  double height;
  double xcoord;
  double ycoord;
  struct _tree * left;
  struct _tree * right;
}TreeNode;

typedef struct _list
{
  int thisnode;
  int parnode;
  int lcnode;
  int rcnode;
  char cutline;
  double width;
  double height;
  struct _list * next;
}ListNode;

TreeNode * load_file(char * filename, int * largest_index);
ListNode * list_construct(int thisnode, int parnode, int lcnode, int rcnode, char cutline, double width, double height);
ListNode * list_insert(ListNode * head, int thisnode, int parnode, int lcnode, int rcnode, char cutline, double width, double height);
void list_destruct(ListNode * head);
TreeNode * tree_construct(ListNode * head);
TreeNode * tree_insert(TreeNode * root,ListNode * head);
TreeNode * tree_search_par(TreeNode * root, int parnode);
void tree_pack(TreeNode * tree);
void rectangle_construct(TreeNode * root);
void get_coordinate(TreeNode * root, TreeNode * parent);
void save_file(char * filename, TreeNode * tree, int total);
TreeNode * tree_search_key(TreeNode * root, int i);
void tree_destruct(TreeNode * tree);
void pre_order(TreeNode * tree);
void in_order(TreeNode * tree);
void post_order(TreeNode * tree);
#endif
