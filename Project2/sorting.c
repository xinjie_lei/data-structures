#include<stdio.h>
#include<stdlib.h>
#include "sorting.h"

Node * Load_File(char * Filename)
{
  //Declaraions
  FILE * fh = NULL;
  long temp = 0;
  Node * list = NULL;
  
  //Statements
  fh = fopen(Filename,"r");
  if(fh == NULL)
    {
      fclose(fh);
      return NULL;
    }
  while(fscanf(fh,"%ld",&temp) != EOF)
    {		 
      list = Node_insert(list, temp);
    } 
  fclose(fh); 
  return list;
}

Node * Node_construct(int value)
{
  //Statements
  Node * n = malloc(sizeof(Node));
  n -> value = value;
  n -> next = NULL;

  return n;
}
Node * Node_insert(Node * list, int temp)
{
  //Declarations
  Node * node = NULL;
  
  //Statements
  if(list == NULL)
    {
      list = Node_construct(temp);
      return list;
    }
  node = Node_construct(temp);
  node -> next = list;
  list = node;

  return list;
}

int * generateSeq(int Size,int * num)
{
  //Declarations
  int n = 0;
  int size = 0;
  int p = 0;
  int q = 0;
  int i = 0;
  int pow_2 = 1;
  int pow_3 = 1;
  int temp = 0;
  int * seq = NULL;
  int ind = 0;

  //Statements
  while(pow_3 < Size)
    {
      pow_3 *= 3;
      i++;
    }
  size = i;
  (*num) = i * (i + 1) / 2 - 1;
  seq = malloc(sizeof(int) * i * (i + 1) / 2);
  for(n = 0; n < size; n++)
    {
      for(p = n,q = 0;p >= 0; p--,q++)
	{
	  pow_2 = 1;
	  for(i = 0; i < p; i++)
	    {
	      pow_2 *= 2;
	    }
	  pow_3 = 1;
	  for(i = 0; i < q; i++)
	    {
	      pow_3 *= 3;
	    }
	  temp = pow_2 * pow_3;
	  seq[ind] = temp;
	  ind++;
	}
    }
  return seq;
}

Node * Shell_Sort(Node * list)
{
  //Declarations
  int * seq = NULL;
  int ind = 0;
  List * list_of_node = NULL;
  int Size = 0;
  Node * head = list;
  
  //Statements
  if(list -> next == NULL || list == NULL)
    {
      return list;
    }
  for(Size = 0; head != NULL; Size ++)
    {
      head = head -> next;
    }
  head = list;
  seq = generateSeq(Size, &ind);
  list_of_node = List_insert(seq[ind]);
  while(ind >= 0)
    {
      if(ind % 2 == 1)
	{
	  list_of_node = insert_d_order(list_of_node, head, seq[ind]);
	}
      else 
	{
	  list_of_node = insert_i_order(list_of_node, head, seq[ind]);
	}
      head = construct_new_list(list_of_node, seq[ind]);
     
      
      ind --;
      }
  free(seq);
  free_list(list_of_node);
  
  return head;
 
}
Node * construct_new_list(List * list_of_node,int k)
{
  //Declarations
  List * head = list_of_node;
  Node * list = list_of_node -> node;
  Node * temp = NULL;
  int i = 1;

  //Statements
  while(head -> node != NULL)
    {
      temp = head -> node;
      head -> node = temp -> next;
      if(i == k)
	{
      	  head = list_of_node;
	  temp -> next = head -> node;
	  i = 1;
	}
      else
	{
	  temp -> next = (head -> next) -> node;
	  head = head -> next;
	  i++;
	} 
    }
  
  return list;
}

List * insert_i_order(List * l_sublist, Node * list,int k)
{
  //Declarations
  List * head = l_sublist;
  Node * temp = NULL;
  Node * u = NULL;
  Node * current = NULL;
  int i = 0;

  //Statements
  for(temp = list; temp != NULL; temp = u)
    {
      u = temp -> next;
      if(i == k)
	{
	  i = 0;
	  head = l_sublist;
	}
      if(head -> node == NULL || (head -> node)-> value >= temp->value )
	{
	  temp -> next = head -> node;
	  head -> node = temp;
	}
      else
	{
	  current = head -> node;
	  while(current -> next != NULL && (current -> next) -> value < temp -> value)
	    {
	      current = current -> next;
	    }
	  temp -> next = current -> next; 
	  current -> next = temp;
	 
	}
      head = head -> next;
      i++;
    }
    
  return l_sublist;
} 

List * insert_d_order(List * l_sublist, Node * list,int k)
{
  //Declarations
  List * head = l_sublist;
  Node * temp = NULL;
  Node * u = NULL;
  Node * current = NULL;
  int i = 0;

  //Statements
  for(temp = list; temp != NULL; temp = u)
    {
      u = temp -> next;
      if(i == k)
	{
	  i = 0;
	  head = l_sublist;
	}
      if(head -> node == NULL || (head -> node)-> value <= temp->value )
	{
	  temp -> next = head -> node;
	  head -> node = temp;
	}
      else
	{
	  current = head -> node;
	  while(current -> next != NULL && (current -> next) -> value > temp -> value)
	    {
	      current = current -> next;
	    }
	  temp -> next = current -> next; 
	  current -> next = temp;
	 
	}
      head = head -> next;
      i++;
    }
    
  return l_sublist;
} 
List * List_construct(Node * list)
{
  //Declarations
  List * l = NULL;

  //Statements
  l = malloc(sizeof(List));
  l -> node = list;
  l -> next = NULL;
  return l;
}

List * List_insert(int n)
{
  //Declarations
  List * node = NULL;
  List * head = NULL;
  int i = 0;

  //Statements
  head = List_construct(NULL);
  for(i = 1; i<n; i++)
    {
      node = List_construct(NULL);
      node -> next = head;
      head = node;
    }
  //head -> node = Node_construct(i);
  return head;

}

int Save_File(char * Filename, Node * list)
{
  //Statements
  FILE * fh = NULL;
  fh = fopen(Filename, "w");
  int written = 0;
  Node * head = list;
  while(head != NULL)
    {
      fprintf(fh, "%ld\n", head -> value);
      head = head -> next;
      written ++;
    }
  free_node(list);
  fclose(fh);
  return written;
}

void free_node(Node * list)
{
  Node * temp = NULL;
  while(list != NULL) 
    {
      temp = list -> next;
      free(list);
      list = temp;  
    }
}
void free_list(List * list)
{
  List * temp = NULL;
  while(list != NULL)
    {
      temp = list -> next;
      free(list);
      list = temp;
    }
}
