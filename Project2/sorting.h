#ifndef SORTING_H
#define SORTING_H
#define CLOCKS_PER_SECOND 1000000

typedef struct _node
{
  long value;
  struct _node *next;
}Node;

typedef struct _list
{
  Node * node;
  struct _list *next;
}List;

Node * Load_File(char * Filename);
int Save_File(char * Filename, Node * list);
int * generateSeq(int Size, int * num);
Node * Shell_Sort(Node * list);
void free_list(List * list);
void free_node(Node * list);
Node * Node_construct(int value);
Node * Node_insert(Node * list, int temp);
Node * construct_new_list(List * list_of_node,int k);
List * insert_d_order(List * l_sublist, Node * list,int k);
List * insert_i_order(List * l_sublist, Node * list,int k);
List * List_construct(Node * list);
List * List_insert(int n);
#endif
