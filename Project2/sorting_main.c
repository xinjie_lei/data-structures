#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include "sorting.h"

int main(int argc, char ** argv)
{
  double time_load = 0;
  double time_save = 0;
  double time_sort = 0;
  Node * list = NULL;

  if(argc != 3)
    {
      printf("usage: ./proj2 input.txt output.txt");
      return EXIT_FAILURE;
    }
  time_load = clock();
  list = Load_File(argv[1]);
  time_load = clock() - time_load;
  time_sort = clock();
  list = Shell_Sort(list);
  time_sort = clock() - time_sort;
  time_save = clock();
  Save_File(argv[2],list);
  time_save = clock() - time_save;

  printf("I/O time: %le\n", (time_load + time_save) / CLOCKS_PER_SECOND);
  printf("Sorting Time: %le\n", time_sort/CLOCKS_PER_SECOND);

  return EXIT_SUCCESS;
}
