#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include "reroot.h"

int main(int argc, char ** argv)
{
    TreeNode * tree = NULL;
    TreeNode * root = NULL;
    TreeNode * found = NULL;
    double time_pack = 0;
    double best_width = 0;
    double best_height = 0;
    
    //Packing
    if(argc != 3)
    {
        printf("Usage : ./proj4 input_file output_file ");
        return EXIT_FAILURE;
    }
    tree = load_file(argv[1]);
    time_pack = clock();
    save_file(argv[2], tree);
    time_pack = clock() - time_pack;

    //Report
    root = tree;
    printf("Preorder:  ");
    pre_order(root);
    printf("\n\n");
    root = tree;
    printf("Inorder:  ");
    in_order(root);
    printf("\n\n");
    root = tree;
    printf("Postorder:  ");
    post_order(root);
    printf("\n\n");
    
    printf("Width: %le\n", tree -> width);
    printf("Height: %le\n\n", tree -> height);
    found = tree_first_triangle(root, NULL);
    printf("X-coordinate: %le\n", found -> xcoord);
    printf("Y-coordinate: %le\n\n", found -> ycoord);
    
    printf("Elapsed Time: %le\n\n", time_pack/CLOCKS_PER_SECOND);

    //Re-rooting
    root = tree;
    time_pack = clock();
    best_width = root -> width;
    best_height = root -> height;
    reroot(tree, root, NULL, NULL, NULL, &best_width, &best_height);
    time_pack = clock() - time_pack;
    printf("Best width: %le\n", best_width);
    printf("Best height: %le\n\n", best_height);
    printf("Elapsed Time for re-rooting: %le\n", time_pack/CLOCKS_PER_SECOND);
  
    tree_destruct(tree);
    
    return EXIT_SUCCESS;
}

TreeNode * load_file(char * filename)
{
    ListNode * head = NULL;
    ListNode * temp = NULL;
    TreeNode * tree = NULL;
    FILE * fh = NULL;
    char temp_c;
    double temp_width;
    double temp_height;
    int i = 0;
    
    fh = fopen(filename, "r");
    while(fscanf(fh, "%c", &temp_c) != EOF)
	{
        if(temp_c == '(')
	    {
            if(fscanf(fh, "%le", &temp_width))
            {}
            fseek(fh, 1, SEEK_CUR);
            if(fscanf(fh, "%le", &temp_height))
            {}
            fseek(fh, 1, SEEK_CUR);
            head = list_insert(head, '-', temp_width, temp_height);
	    }
        else
	    {
            head = list_insert(head, temp_c, 0, 0);
	    }
	}
    temp = head;
    tree = BST_construct(tree, temp, temp, &i);
    
    list_destruct(head);
    
    fclose(fh);
    return tree;
}
ListNode * list_construct(char temp_c, double temp_width, double temp_height)
{
    ListNode * node = NULL;
    node = malloc(sizeof(ListNode));
    node -> cutline = temp_c;
    node -> width = temp_width;
    node -> height = temp_height;
    node -> next = NULL;
    
    return node;
}
ListNode * list_insert(ListNode * head, char temp_c, double temp_width, double temp_height)
{
    ListNode * node = NULL;
    node = list_construct(temp_c, temp_width, temp_height);
    
    node -> next = head;
    head = node;
    
    return head;
}

void list_destruct(ListNode * list)
{
    ListNode * temp = NULL;
    while(list != NULL)
    {
        temp = list -> next;
        free(list);
        list = temp;
    }
}
TreeNode * tree_construct(ListNode * list)
{
    TreeNode * node = NULL;
    node = malloc(sizeof(TreeNode));
    if(list -> cutline ==  '-')
    {
        node -> width = list -> width;
        node -> height = list -> height;
        node -> cutline = '-';
    }
    else
    {
        node -> width = 0;
        node -> height = 0;
        node -> cutline = list -> cutline;
    }
    node -> xcoord = 0;
    node -> ycoord = 0;
    node -> left = NULL;
    node -> right = NULL;
    return node;
}

TreeNode * BST_construct(TreeNode * tree, ListNode * list, ListNode * temp, int * i)
{
    if(temp -> cutline == '-')
    {
        (*i)++;
        return tree_construct(temp);
    }
    
    tree = tree_construct(temp);
    (*i)++;
    
    temp = list_shift(list, i);
    tree -> right = BST_construct(tree -> right, list, temp, i);
    temp = list_shift(list, i);
    tree -> left = BST_construct(tree -> left, list, temp, i);
    return tree;
}

ListNode * list_shift(ListNode * list, int * i)
{
    ListNode * temp = list;
    int j = 0;
    for(j = 0; j < (*i); j++)
    {
        temp = temp -> next;
    }
    return temp;
}

void rectangle_construct(TreeNode * root)
{
    //Statements
    if(root -> cutline == '-')
    {
        return;
    }
    rectangle_construct(root -> left);
    rectangle_construct(root -> right);
    if(root -> cutline == 'V')
    {
        root -> width = root -> left -> width +  root -> right -> width;
        if(root -> left -> height > root -> right -> height)
        {
            root -> height = root -> left -> height;
        }
        else
        {
            root -> height = root -> right -> height;
        }
    }
    else
    {
        root -> height = root -> left -> height +  root -> right -> height;
        if(root -> left -> width > root -> right -> width)
        {
            root -> width = root -> left -> width;
        }
        else
        {
            root -> width = root -> right -> width;
        }
    }
}
void get_coordinate(TreeNode * root, TreeNode * parent)
{
    
    //Statements
    if(root == NULL)
    {
        return;
    }
    if(parent == NULL)
    {
        root -> xcoord = 0;
        root -> ycoord = 0;
    }
    else
    {
        if(parent -> cutline == 'H')
        {
            if(root == parent -> left)
            {
                parent -> left -> xcoord = parent -> xcoord;
                parent -> left -> ycoord = parent -> ycoord +  parent -> height - parent -> left -> height;
            }
            else
            {
                parent -> right -> xcoord = parent -> xcoord;
                parent -> right -> ycoord = parent -> ycoord;
            }
        }
        else
        {
            if(root == parent -> left)
            {
                parent -> left -> xcoord = parent -> xcoord;
                parent -> left -> ycoord = parent -> ycoord;
            }
            else
            {
                parent -> right -> xcoord = parent -> xcoord + parent -> left -> width;
                parent -> right -> ycoord = parent -> ycoord;
            }
        }
    }
    get_coordinate(root -> left, root);
    get_coordinate(root -> right, root);
}

void reroot(TreeNode * tree, TreeNode * root, TreeNode * parent, TreeNode * ancestor, TreeNode * pre_anc, double * b_width, double * b_height)
{
  if(parent != NULL && root == NULL)
    {
      return;
    }
  construct(tree, root, parent, ancestor, pre_anc,b_width, b_height);
  reroot(tree, root -> left, root, parent, ancestor, b_width, b_height);
  reroot(tree, root -> right, root, parent, ancestor, b_width, b_height);
  de_construct(tree, root, parent, ancestor, pre_anc);
}
void construct(TreeNode * tree, TreeNode * root, TreeNode * parent, TreeNode * ancestor, TreeNode * pre_anc, double * b_width, double * b_height)
{
  if(parent == NULL || parent == tree)
    {
      return;
    }
  if(root == parent -> left && parent == ancestor -> left)
    {
      if(ancestor == tree)
	{
	  helper(ancestor,parent -> right, ancestor -> right);
	}
      else
	{
	  helper(ancestor,parent -> right, pre_anc);
	}
    }
  else if(root == parent -> right && parent == ancestor -> left)
    {
      if(ancestor == tree)
	{
	  helper(ancestor, parent -> left, ancestor -> right);
	}
      else
	{
	  helper(ancestor, parent -> left, pre_anc);
	}
    }
  else if(root == parent -> left && parent == ancestor -> right)
    {
      if(ancestor == tree)
	{
	  helper(ancestor, ancestor -> left, parent -> right);
	}
      else
	{
	  helper(ancestor, pre_anc, parent -> right);
	}
    }
  else
    {
      if(ancestor == tree)
	{
	  helper(ancestor, ancestor -> left, parent -> left);
	}
      else
	{
	  helper(ancestor, pre_anc, parent -> left);
	}
    }
  helper(parent, root, ancestor);
  if((parent -> width * parent -> height) < ((*b_width) * (*b_height)))
    {
      *b_width = parent -> width;
      *b_height = parent -> height;
    }
  if((parent -> width * parent -> height) == ((*b_width) * (*b_height)))
    {
      if(parent -> width < (*b_width))
	{
	  (*b_width) = parent -> width;
	  (*b_height) = parent -> height;
	}
    }
}

void de_construct(TreeNode * tree,TreeNode * root,  TreeNode * parent, TreeNode * ancestor, TreeNode * pre_anc)
{
  if(parent == tree || parent == NULL)
    {
      return;
    }
  helper(parent, parent -> left, parent -> right);
  if(pre_anc != NULL)
    {
      helper(ancestor, parent, pre_anc);
    }
  else if(pre_anc == NULL && parent == ancestor -> left)
    {
      helper(ancestor, parent, ancestor -> right);
    }
  else
    {
      helper(ancestor, parent, ancestor -> left);
    }
}
void helper(TreeNode * root, TreeNode * lc, TreeNode * rc)
{
 if(root -> cutline == 'V')
    {
        root -> width = lc -> width + rc -> width;
        if(lc -> height >= rc -> height)
        {
            root -> height = lc -> height;
        }
        else
        {
            root -> height = rc -> height;
        }
    }
 else
   {
     root -> height = lc-> height +  rc -> height;
     if(lc -> width >= rc -> width)
       {
	 root -> width = lc -> width;
       }
     else
       {
	 root -> width = rc -> width;
       }
   }
}
void save_file(char * filename, TreeNode * tree)
{
  //Declarations
  FILE * fh = NULL;
  TreeNode * root = tree;

  //Statements
  fh = fopen(filename, "w");
  rectangle_construct(root);
  root = tree;
  get_coordinate(root, NULL);
  root = tree;
  save_tree_to_file(fh, root);
  fclose(fh);
  return;
}
void save_tree_to_file(FILE * fh, TreeNode * root)
{
  if(root == NULL)
    {
      return;
    }
  save_tree_to_file(fh, root -> left);
  save_tree_to_file(fh, root -> right);
  if(root -> cutline == '-')
    {
      fprintf(fh, "%le %le %le %le\n", root -> width, root -> height, root -> xcoord, root -> ycoord);
    }
}

TreeNode * tree_first_triangle(TreeNode * root, TreeNode * parent)
{
  if(root -> cutline == '-')
    {
      if(parent -> xcoord == 0 && parent -> ycoord == 0)
	{
	  return root;
	}
      else
	{
	  return parent;
	}
    }
  return tree_first_triangle(root -> left, root);
}

void tree_destruct(TreeNode * tree)
{
    //Statements
    if(tree == NULL)
    {
        return;
    }
    tree_destruct(tree -> left);
    tree_destruct(tree -> right);
    free(tree);
}
void pre_order(TreeNode * tree)
{
    //Statements
    if(tree == NULL)
    {
        return;
    }
    if(tree -> cutline == '-')
    {
        printf("(%le,%le)", tree -> width, tree -> height);
    }
    else
    {
        printf("%c", tree -> cutline);
    }
    pre_order(tree -> left);
    pre_order(tree -> right);
}
void in_order(TreeNode * tree)
{
    //Statements
    if(tree == NULL)
    {
        return;
    }
    in_order(tree -> left);
    if(tree -> cutline == '-')
    {
        printf("(%le,%le)", tree -> width, tree -> height);
    }
    else
    {
        printf("%c", tree -> cutline);
    }
    in_order(tree -> right);
}
void post_order(TreeNode * tree)
{
    //Statements
    if(tree == NULL)
    {
        return;
    }
    post_order(tree -> left);
    post_order(tree -> right);
    if(tree -> cutline == '-')
    {
        printf("(%le,%le)", tree -> width, tree -> height);
    }
    else
    {
        printf("%c", tree -> cutline);
    }
}
