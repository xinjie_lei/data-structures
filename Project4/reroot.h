#ifndef REROOT_H
#define REROOT_H
#define CLOCKS_PER_SECOND 1000000

typedef struct _tree
{
  char cutline;
  double width;
  double height;
  double xcoord;
  double ycoord;
  struct _tree * left;
  struct _tree * right;
}TreeNode;

typedef struct _list
{
  char cutline;
  double width;
  double height;
  struct _list * next;
}ListNode;

TreeNode * load_file(char * filename);
ListNode * list_construct(char temp_c, double temp_width, double temp_height);
ListNode * list_insert(ListNode * head, char temp_c, double temp_width, double temp_height);
void list_destruct(ListNode * list);
TreeNode * tree_construct(ListNode * list);
TreeNode * BST_construct(TreeNode * tree, ListNode * list, ListNode * temp, int * i);
ListNode * list_shift(ListNode * list, int * i);
void tree_destruct(TreeNode * tree);
void pre_order(TreeNode * tree);
void in_order(TreeNode * tree);
void post_order(TreeNode * tree);
void save_tree_to_file(FILE * fh, TreeNode * root);
void save_file(char * filename, TreeNode * tree);
void get_coordinate(TreeNode * root, TreeNode * parent);
void rectangle_construct(TreeNode * root);
TreeNode * tree_first_triangle(TreeNode * root, TreeNode * parent);
void de_construct(TreeNode * tree,TreeNode * root,  TreeNode * parent, TreeNode * ancestor, TreeNode * pre_anc);
void construct(TreeNode * tree, TreeNode * root, TreeNode * parent, TreeNode * ancestor, TreeNode * pre_anc, double * b_width, double * b_height);
void reroot(TreeNode * tree, TreeNode * root, TreeNode * parent, TreeNode * ancestor, TreeNode * pre_anc, double * b_width, double * b_height);
void helper(TreeNode * root, TreeNode * lc, TreeNode * rc);
#endif
